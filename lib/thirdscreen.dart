import 'package:assignment/forthscreen.dart';
import 'package:flutter/material.dart';

class ThirdScreen extends StatelessWidget {
  const ThirdScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Positioned(
              left: 0,
              right: 0,
              child: Container(
                width: double.maxFinite,
                height: 230,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/th.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Positioned(
              left: 20,
              top: 10,
              child: Row(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.menu),
                    color: Colors.white,
                  )
                ],
              ),
            ),
            Positioned(
              top: 200,
              bottom: 200,
              child: Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                height: 200,
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    'Search for partner',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 250,
              child: Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                height: 300,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                child: ListView.builder(
                  itemCount: 10,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Row(
                      children: [
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Card(
                                child: Container(
                                  child: Image.asset(
                                    'assets/images/doctor.jpg',
                                  ),
                                  height: 50,
                                ),

                              ),
                            ),
                            Text("Doctor")
                          ],
                        ),
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Card(
                                elevation: 5,
                                child: Container(
                                  child: Image.asset(
                                    'assets/images/engineer.jpg',
                                    width: 50,
                                    height: 50,
                                  ),
                                  height: 50,
                                ),
                              ),
                            ),
                            Text("Engineer")
                          ],
                        ),
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Card(
                                child: Container(
                                  child: Image.asset(
                                    'assets/images/teacher.png',
                                  ),
                                  height: 50,
                                ),
                              ),
                            ),
                            Text("Teacher"),
                          ],
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top :360.0,left: 20),
              child: Text('Latest Profile', style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 360.0,right: 20),
              child: InkWell(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                      return ForthScreen();
                    },
                    ));
                  },
                  child: Text('See ALL',textAlign: TextAlign.right,)
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 390.0,left: 20),
              child: Positioned(child: Expanded(
                child: Container(
                  child: ListView.builder(
                    itemCount: 6,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                width: 200,
                                height: 200,
                                child: Card(
                                  child: Column(
                                    children: [
                                      CircleAvatar(
                                        foregroundImage:
                                        AssetImage('assets/images/g2.jpeg'),
                                        radius: 30,
                                      ),
                                      Text(
                                        '24',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.pink,
                                        ),
                                      ),
                                      Text(
                                        'Years Old',
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Text(
                                        'Rachel Green',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(
                                        'Fashion Designer',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                width: 200,
                                height: 200,
                                child: Card(
                                  child: Column(
                                    children: [
                                      CircleAvatar(
                                        foregroundImage:
                                        AssetImage('assets/images/g4.jpg'),
                                        radius: 30,
                                      ),
                                      Text(
                                        '23',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.pink,
                                        ),
                                      ),
                                      Text(
                                        'Years Old',
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Text(
                                        'Monika Green',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(
                                        'Fashion Designer',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                width: 200,
                                height: 200,
                                child: Card(
                                  child: Column(
                                    children: [
                                      CircleAvatar(
                                        foregroundImage:
                                        AssetImage('assets/images/g2.jpeg'),
                                        radius: 30,
                                      ),
                                      Text(
                                        '24',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.pink,
                                        ),
                                      ),
                                      Text(
                                        'Years Old',
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Text(
                                        'Rachel Green',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(
                                        'Fashion Designer',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 570.0),
              child: BottomAppBar(
                color: Colors.red,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:[
                      Padding(
                        padding: const EdgeInsets.only(left: 60.0),
                        child: IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.heart_broken_rounded,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 60.0),
                        child: IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.add_alert,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 60.0),
                        child: IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.home,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 60.0),
                        child: IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.message_sharp,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:60.0),
                        child: IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.search,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
