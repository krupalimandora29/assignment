import 'package:assignment/login.dart';
import 'package:flutter/material.dart';

class ForthScreen extends StatelessWidget {
  const ForthScreen({super.key});

  get bottomNavigationBar => null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(50),
                bottomRight: Radius.circular(50),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "My Favorite Profile",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              child: ListView.builder(
                itemCount: 2,
                scrollDirection: Axis.vertical,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 200,
                            height: 200,
                            child: Card(
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 130.0),
                                    child: CircleAvatar(
                                      foregroundImage:
                                      AssetImage('assets/images/g1.jpg'),
                                      radius: 30,
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      '24',
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.pink,
                                      ),
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      'Years Old',
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Rachel Green',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    'Fashion Designer',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: 200,
                            height: 200,
                            child: Card(
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 130.0),
                                    child: CircleAvatar(
                                      foregroundImage:
                                      AssetImage('assets/images/g2.jpeg'),
                                      radius: 30,
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      '24',
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.pink,
                                      ),
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      'Years Old',
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Rachel Green',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    'Fashion Designer',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 200,
                            height: 200,
                            child: Card(
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 130.0),
                                    child: CircleAvatar(
                                      foregroundImage:
                                      AssetImage('assets/images/g4.jpg'),
                                      radius: 30,
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      '24',
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.pink,
                                      ),
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      'Years Old',
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Monika Green',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    'Fashion Designer',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: 200,
                            height: 200,
                            child: Card(
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 130.0),
                                    child: CircleAvatar(
                                      foregroundImage:
                                      AssetImage('assets/images/g2.jpeg'),
                                      radius: 30,
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      '24',
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.pink,
                                      ),
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      'Years Old',
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Rachel Green',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    'Fashion Designer',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
          BottomAppBar(
            color: Colors.red,
            // BottomAppBar content
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children:[
                  Padding(
                    padding: const EdgeInsets.only(left: 60.0),
                    child: IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.heart_broken,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 60.0),
                    child: IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.add_alert,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 60.0),
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context){
                          return LoginRoute();
                        },));
                      },
                      icon: Icon(
                        Icons.home,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 60.0),
                    child: IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.message_sharp,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left:60.0),
                    child: IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.search,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
