import 'package:assignment/register.dart';
import 'package:flutter/material.dart';

class LoginRoute extends StatefulWidget {
  @override
  State<LoginRoute> createState() => _LoginRouteState();
}

class _LoginRouteState extends State<LoginRoute> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController userNameController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  bool isPasswordVisible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            'assets/images/img2.png',
            fit: BoxFit.cover,
          ),
          Container(
            color: Colors.black54,
          ),
          Row(
            children: [
              Expanded(
                child: Container(),
              ),
              Expanded(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Soulmate,\nmatrimonay app',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                      Container(
                        child: TextFormField(
                          controller: userNameController,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Enter UserName';
                            } else if (value.length < 10) {
                              return 'Enter Valid UserName';
                            }
                          },
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                            ),
                            prefixIcon: Icon(
                              Icons.supervised_user_circle,
                            ),
                            suffixIcon: InkWell(
                              onTap: () {
                                userNameController.clear();
                              },
                              child: Icon(
                                Icons.clear,
                                size: 20,
                                color: Colors.grey,
                              ),
                            ),
                            hintText: 'Enter UserName',
                          ),
                        ),
                        color: Colors.white,
                      ),
                      Container(
                        child: TextFormField(
                          controller: passwordController,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Enter Password';
                            }
                          },
                          obscureText: !isPasswordVisible,
                          obscuringCharacter: '#',
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                            ),
                            prefixIcon: Icon(
                              Icons.lock,
                            ),
                            suffixIcon: InkWell(
                              onTap: () {
                                setState(() {
                                  isPasswordVisible = !isPasswordVisible;
                                });
                              },
                              child: Icon(
                                isPasswordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                size: 20,
                                color: Colors.grey,
                              ),
                            ),
                            hintText: 'Enter Password',
                          ),
                        ),
                        color: Colors.white,
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          top: 10,
                          bottom: 10,
                        ),
                        color: Colors.pink,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextButton(
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  print(
                                      'VALUE INSIDE USERNAME : ${userNameController.text.toString()}');
                                  print(
                                      'VALUE INSIDE PASSWORD : ${passwordController.text.toString()}');
                                  Navigator.of(context).pop();
                                }
                              },
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) {
                                      return Signin();
                                    },
                                  ));
                                },
                                child: Center(
                                  child: Text(
                                    'Log In',
                                    style: TextStyle(fontSize: 18.0, color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(),
              ),
            ],

          ),
        ],
      ),
    );
  }
}
